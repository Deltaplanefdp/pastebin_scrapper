__author__ = '{Mathias Vuitton, Olivier Miesch, Helene Voilly, Erwan Le Blavec}'
__copyright__ = 'Copyright {2020}, {Pastebin_Scrapper}'
__license__ = '{do What The Fuck you want to Public License}'
__version__ = '{1}.{4}.{1}'
__maintainer__ = '{Erwan Le Blavec}'

import requests
import time
from bs4 import BeautifulSoup
import re
import json
from stem import Signal
from stem.control import Controller
import datetime
import elk_connexion
import cfscrape


class Pastebin:

    def check_proxy_working(self):
        """Check if the proxy is working and the proxy timeout"""
        session = requests.session()
        session.proxies["http"] = "socks5h://localhost:9050"
        session.proxies["https"] = "socks5h://localhost:9050"
        dt_started = datetime.datetime.utcnow()
        try:
            request = session.get("http://httpbin.org/ip")
            print(request.text)
            dt_ended = datetime.datetime.utcnow()
        except IOError:
            return False
        if (dt_ended - dt_started) > datetime.timedelta(days=0, seconds=2, microseconds=0):
            return False
        else:
            return True

    def get_page_html(self, url):
        """Return an html page from a given URL"""
        #Would work if pastebin wasn't protected by cloudflare
        """session = requests.session()
        session.proxies["http"] = "socks5h://localhost:9050"
        session.proxies["https"] = "socks5h://localhost:9050"
        session.headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0"}
        scraper = cfscrape.create_scraper(sess=session)
        session = scraper.get(url)
        print(session.text)"""

        result = requests.get(url).text
        return result

    def get_scrapping_url_archive(self, page):
        """Scrape the archive page"""
        tableau = []
        # We exclude this links for the parsing
        exception = ["/pro", "/api", "/tools", "/faq", "https://deals.pastebin.com", "/login", "/signup", "/archive",
                     "/archive/lua", "/archive/javascript", "/archive/csharp", "/archive/javascript",
                     "/archive/html4strict", "/archive/python", "/archive/bash", "/archive/json", "/archive/csharp",
                     "/archive/cpp", "/doc_cookies_policy", "/tools#chrome", "/tools#firefox", "/tools#iphone",
                     "/tools#windows", "/tools#android", "/tools#macos", "/tools#opera", "/tools#pastebincl",
                     "/", "/languages", "/night_mode", "/doc_scraping_api", "/doc_privacy_statement",
                     "/doc_terms_of_service", "/doc_security_disclosure", "/dmca", "/contact", "/archive/java",
                     "http://creativecommons.org/licenses/by-sa/3.0/", "https://favpng.com",
                     "http://steadfast.net/services/dedicated-servers.php?utm_source=pastebin.com&utm_medium=referral"
                     "&utm_content=footer_link_dedicated_server_hosting_by&utm_campaign"
                     "=referral_20140118_x_x_pastebin_partner&source=referral_20140118_x_x_pastebin_partner",
                     "http://steadfast.net/?utm_source=pastebin.com&utm_medium=referral&utm_content"
                     "=footer_link_steadfast&utm_campaign=referral_20140118_x_x_pastebin_partner&source"
                     "=referral_20140118_x_x_pastebin_partner",
                     "https://facebook.com/pastebin",
                     "https://twitter.com/pastebin",
                     "/archive/xml",
                     "/archive/c",
                     "/archive/apache",
                     "/archive/php",
                     "/archive/powershell",
                     "/archive/css",
                     "#0",
                     ]
        # We get all the links in the page then exclude the useless links
        try:
            soup = BeautifulSoup(page, features="html.parser")
            for a in soup.find_all('a', href=True):
                if a['href'] not in exception:
                    tableau.append(a['href'])
            print(tableau)
            return tableau
        except TypeError:
            print("Error")
            pass

    def create_link(self, results):
        """Create the page link to be parsed (pastebin.com+unique ID)"""
        tableau = []
        pastebin = "https://pastebin.com"
        for i in results:
            url = ''.join([pastebin, i])
            tableau.append(url)
        return tableau

    def parse_page(self, urls):
        """Parse the pastebin page to retrieve every page with important words.
        Then extract some fields via regex"""
        tableau = []
        domaines = []
        dict = {}
        tab = ["orange", "livebox", "stephane richard", "business services"]
        for i in urls:
            time.sleep(1)
            soup = BeautifulSoup(self.get_page_html(i), features="html.parser")
            print(i)
            try:
                for j in tab:
                    if j in str(soup).lower():
                        for textarea in soup.find('textarea'):
                            dict["timestamp"] = time.time()
                            dict["ipConcerned"] = re.findall(
                                "(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})", textarea)
                            dict["mailAddress"] = re.findall("[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,3}", textarea)
                            domains = re.findall("([a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])\.)*orange\.com$",
                                                 textarea)
                            for k in domains:
                                domaines.append(k)
                            dict["domainName"] = domaines
                            dict["cuid"] = re.findall("[A-Z]{4}[0-9]{4}", textarea)
                            dict["url"] = i
                            elk_connexion.store_record(elk_connexion.connect_elasticsearch(), 'orange', json.dumps(dict))
                            tableau.append(dict)
            except TypeError:
                pass
        return tableau

    @staticmethod
    def renew_tor_ip():
        with Controller.from_port(port=9051) as controller:
            controller.authenticate(password="MyStr0n9P#D")
            controller.signal(Signal.NEWNYM)


if __name__ == '__main__':
    """Main function, call all the fonctions in the file.
    It will check that the proxy  is working, then get the pastebin page.
    Parse the pastebin page and store the parsed ddata in elasticsearch"""
    p = Pastebin()
    while True:
        p.renew_tor_ip()
        p.check_proxy_working()
        page = p.get_page_html('https://pastebin.com/archive')
        time.sleep(2)
        results = p.get_scrapping_url_archive(page)
        urls = p.create_link(results)
        for i in p.parse_page(urls):
            print(json.dumps(i))
