__author__ = '{Mathias Vuitton, Olivier Miesch, Helene Voilly, Erwan Le Blavec}'
__copyright__ = 'Copyright {2020}, {Pastebin_Scrapper}'
__license__ = '{do What The Fuck you want to Public License}'
__version__ = '{1}.{4}.{1}'
__maintainer__ = '{Erwan Le Blavec}'


from elasticsearch import Elasticsearch


def connect_elasticsearch():
    _es = None
    _es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if _es.ping():
        print('Connexion à Elasticsearch : Réussie')
    else:
        print('Connexion à Elasticsearch : Echec')
    return _es


def store_record(elastic_object, index_name, record):
    try:
        elastic_object.index(index=index_name, doc_type='orange', body=record)
    except Exception as ex:
        print('Connexion à Elasticsearch : Erreur Indexation')
        print(str(ex))